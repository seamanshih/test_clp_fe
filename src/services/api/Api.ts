import axios, { AxiosResponse } from "axios";
import BaseApi from "./BaseApi";
import config from "../../config";

export enum Type {
  BLUE,
  ORANGE
}

export default class Api extends BaseApi {
  public postType = (button: string): Promise<AxiosResponse<any>> => {
    const body = {
      query: `
        mutation ($type: Type!) {
          createGame (
            type: $type
          ) {
            id
            type
            timestamp
          }
        }
      `,
      variables: {
        type: button
      }
    }

    return axios.
      post(`${config.API}/graphql`, body)
      .then(response => response.data)
      .catch(error => {
        if (error.response) {
          throw error.response.data;
        }
        throw error;
      });
  }

  public postClean = (): Promise<AxiosResponse<any>> => {
    const body = {
      query: `
        mutation {
          resetGame
        }
      `
    }

    return axios.
      post(`${config.API}/graphql`, body)
      .then(response => response.data)
      .catch(error => {
        if (error.response) {
          throw error.response.data;
        }
        throw error;
      });
  }
}