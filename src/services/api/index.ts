import config from "../../config";
import Api from "./Api";

const basicApi = new Api({
  options: {
    host: config.API,
    version: "",
  },
});

export {
  Api,
  basicApi,
}