import axios, { AxiosRequestConfig } from "axios";

export interface ApiConfig {
  options: {
    host: string;
    version?: string;
  };
}

export default class BaseApi {
  protected apiEndpoint: string;

  constructor(apiConfig: ApiConfig) {
    this.apiEndpoint = `${apiConfig.options.host}/api/${apiConfig.options.version}`;
    this.apiCall = this.apiCall.bind(this);
  }

  protected async apiCall(method: AxiosRequestConfig["method"], url: AxiosRequestConfig["url"], data?: AxiosRequestConfig["data"], accessToken?: string): Promise<any> {
    const request: AxiosRequestConfig = {
      method,
      url: `${this.apiEndpoint}${url}`,
      data,
    };

    if (accessToken) {
      request.headers = {
        common: {
          Authorization: `Bearer ${accessToken}`,
        },
      };
    }

    const response = await axios(request);
    return response;
  }
}
