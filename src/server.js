const next = require('next');
const helmet = require("helmet");
const express = require("express");

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();


app.prepare().then(() => {
  const server = express();

  server.use(helmet.xssFilter());
  server.use(helmet.frameguard());

  server.get('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(8080, (err) => {
    if (err) throw err
    console.log('> Ready on http://localhost:8000');
  });
})
  .catch((ex) => {
    console.error(ex.stack)
    process.exit(1)
  });