import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });
import LayoutPage from './index';

jest.mock('react-i18next', () => ({
    useTranslation: () => ({ t: (key: string) => key })
}));

describe('LayoutPage', () => {
    it('should render without throwing an error', function () {
        const wrap = mount(<LayoutPage />);
        expect(wrap.contains(<LayoutPage />)).toBe(true)
    })
})