import Head from "next/head";
import React from "react";
import { NextPage } from 'next';
import { useTranslation } from "react-i18next";

const Layout: NextPage = ({ children }) => {
  const { t } = useTranslation();
  return (
    <React.Fragment>
      <Head>
        <title>{t("Test Website")}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      {children}
    </React.Fragment>
  )
}

export default Layout;
