import { Action } from "redux";
import { Map } from "immutable";

export interface ApplicationActionInput {
  locale: string;
  type: string;
}

export interface ApplicationState extends ApplicationActionInput {
}

export type ApplicationAction = Action & ApplicationActionInput;

export type ImmutedApplicationState = Map<ApplicationState, ApplicationState>;