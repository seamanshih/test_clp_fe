export interface Token {
  access_token: string;
  token_type: string;
  expires_in: number;
  state: string;
}

export default interface AuthStorage {
  setToken(token: Token): Promise<Token>;
  getToken(): Promise<Token | null>;
  clearToken(): Promise<void>;
}
