
export enum NOTIFY_RELEASE_STAGES_TYPES {
  production = "production",
  staging = "staging",
  development = "development",
}

export type Language = "zh" | "en";

export interface AppConfig {
  notifyReleaseStagesTypes: NOTIFY_RELEASE_STAGES_TYPES[];
  API: string;
  WS: string;
}