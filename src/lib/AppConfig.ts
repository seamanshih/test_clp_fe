import * as TypesLib from "./types";
import config from "../config";

const appConfig: TypesLib.AppConfig = {
  notifyReleaseStagesTypes: [
    TypesLib.NOTIFY_RELEASE_STAGES_TYPES.production,
    TypesLib.NOTIFY_RELEASE_STAGES_TYPES.staging,
  ],
  API: config.API,
  WS: config.WS,
}

export default appConfig;