import jest from 'jest';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });
import HomePage from './index';

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: (key: string) => key })
}));

describe('HomePage', () => {
  it('should render without throwing an error', function () {
    const wrap = mount(<HomePage />);
    expect(wrap.find('div').text()).toBe('');
  })
})