import * as React from "react";
import { NextPage } from 'next';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Label } from 'recharts';
import { useSelector, useDispatch } from "react-redux";
import { Container, Row, Col } from 'react-bootstrap';
import { useSubscription, gql } from "@apollo/client";
import { ApplicationActions } from "../../reducers/application";

const DashboardPage: NextPage = () => {
  const pageLoad = () => {
    dispatch(ApplicationActions.cleanRecord());
  }
  React.useEffect(() => {
    //component will unmount
    window.addEventListener("load", pageLoad);
    return () => {
      window.removeEventListener("load", pageLoad);
    }
  });

  const dispatch = useDispatch()
  const state: any = useSelector(state => state);
  const { data } = useSubscription(
    gql`
      subscription onGameAdded {
        gameAdded {
          id
          type
          timestamp
        }
      }
    `
  );

  let orange = 0
  let blue = 0
  let chartdata: any[] = []
  let frequency = 0.5
  let maxSec = 5
  for (let i = 0; i <= maxSec; i++) {
    chartdata.push({
      name: i,
      orange: 0,
      blue: 0
    })
  }

  if (data) {
    chartdata = []
    dispatch(ApplicationActions.updateRecord(data));
    const application = state.toJS().application
    orange = application.record.filter(item => item.type === 'ORANGE').length || 0
    blue = application.record.filter(item => item.type === 'BLUE').length || 0
    const startTime = Math.min(...application.record.map(o => o.timestamp));
    for (let i = 0; i <= maxSec; i += frequency) {
      const leftAddTime = Number(startTime + (i * 1000))
      const rightAddTime = Number(leftAddTime + (frequency * 1000))
      const timeInSlot = application.record.filter(item => item.timestamp >= leftAddTime && item.timestamp <= rightAddTime)
      const orangeNumber = timeInSlot.filter(item => item.type === 'ORANGE').length || 0
      const blueNumber = (timeInSlot.length - orangeNumber) || 0
      chartdata.push({
        name: i,
        orange: orangeNumber,
        blue: blueNumber
      })
    }
  }

  const renderLineChart = (
    <LineChart width={1000} height={300} data={chartdata}>
      <Line type="monotone" dataKey="orange" stroke="#ff9559" />
      <Line type="monotone" dataKey="blue" stroke="#007aff" />
      <CartesianGrid stroke="#ccc" />
      <XAxis dataKey="name">
        <Label value="Seconds(s)" offset={0} position="insideBottom" />
      </XAxis>
      <YAxis dataKey="name" label={{ value: 'Clicks(s)', angle: -90, position: 'insideLeft' }} />
    </LineChart>
  );

  return (
    <Container className="Dashboard" >
      <Row>
        <Col xs={12} md={12} className="text-center">
          {renderLineChart}
        </Col>
      </Row>
      <Row className="Dashboard__Result">
        <Col xs={12} md={6} className="text-center">
          <div className="Dashboard__Result--orange">{orange}</div>
        </Col>
        <Col xs={12} md={6} className="text-center">
          <div className="Dashboard__Result--blue">{blue}</div>
        </Col>
      </Row>
    </Container>
  )
}

export default DashboardPage;