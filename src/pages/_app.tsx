import { NextPageContext } from "next";
import NextApp, { AppProps, AppContext } from "next/app";
import { END } from "redux-saga";
import * as React from "react";
import { ApolloProvider } from '@apollo/react-hooks';
import { wrapper, SagaStore } from "../reducers/store";
import withApollo from '../lib/initApollo';
import { Layout } from "../containers";
import "../i18n";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/main.css';

export interface HomeProps {
  locale: string;
}

export interface ApplicationContext extends AppContext, NextPageContext { }

interface Props extends AppProps, HomeProps {
  store: any;
  apollo: any;
  isServer: boolean;
}

const App = ({ Component, pageProps, apollo }: Props) => {
  return (
    <ApolloProvider client={apollo}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ApolloProvider>
  );
}

App.getInitialProps = async (appContext: ApplicationContext) => {
  const appProps = await NextApp.getInitialProps(appContext);

  const { Component, ctx } = appContext;
  const isServer = !!ctx.req;
  // const { locale } = query;
  // 1. Wait for all page actions to dispatch
  const pageProps = {
    ...appProps,
    ...(Component.getInitialProps ? await Component.getInitialProps(ctx) : {}),
  };
  // 2. Stop the saga if on server
  if (isServer) {
    if (ctx.store) {
      const sagaStore = (ctx.store as SagaStore);
      // ctx.store.dispatch(ApplicationActions.initApplication());
      // sagaStore.dispatch(ApplicationActions.getOptions());
      // store.dispatch(OptionActions.getOptions(mapToServerLocale(AppLocale)));
      sagaStore.dispatch(END);
      if (sagaStore.sagaTask) {
        await sagaStore.sagaTask!.toPromise();
      }
    }
  }
  // 3. Return props
  return { pageProps }
}

export default wrapper.withRedux(withApollo(App));