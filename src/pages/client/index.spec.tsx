import jest from 'jest';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });
import ClientPage from './index';

jest.mock('react-i18next', () => ({
    useTranslation: () => ({ t: (key: string) => key })
}));
jest.mock("react-redux", () => ({
    useSelector: jest.fn(fn => fn()),
    useDispatch: () => jest.fn()
}));

describe('ClientPage', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    afterAll(() => {
        jest.restoreAllMocks();
    });

    it('should render without throwing an error', function () {
        const wrap = mount(<ClientPage />);
        expect(wrap.contains(<ClientPage />)).toBe(true)
    })
})