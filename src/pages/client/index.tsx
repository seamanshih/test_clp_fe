import * as React from "react";
import { NextPage } from 'next';
import { useDispatch } from "react-redux";
import { Container, Row, Col, Button } from 'react-bootstrap';
import { ApplicationActions } from "../../reducers/application";


const ClientPage: NextPage = () => {
  const dispatch = useDispatch()

  const handleClick = (type: string) => {
    dispatch(ApplicationActions.postType(type));
  }

  return (
    <Container className="Client" >
      <Row>
        <Col xs={12} md={6} className="text-center">
          <Button className="Client__Button" variant="warning" size="lg" onClick={() => handleClick('ORANGE')}>-</Button>
        </Col>
        <Col xs={12} md={6} className="text-center">
          <Button className="Client__Button" variant="primary" size="lg" onClick={() => handleClick('BLUE')}>+</Button>
        </Col>
      </Row>
    </Container>
  )
}


export default ClientPage;
