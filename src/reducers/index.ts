import { combineReducers } from "redux-immutable";
import { Map } from "immutable";
import { ApplicationReducer } from "./application";

export interface rootState extends Map<string, any> {
  application: any,
}

const rootReducer = combineReducers({
  application: ApplicationReducer,
})

export default rootReducer;
