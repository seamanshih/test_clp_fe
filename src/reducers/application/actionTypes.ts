import { createActions } from "reduxsauce";

export const { Types, Creators } = createActions({
  initApplication: ["locale"],
  postType: ["button"],
  updateRecord: ["data"],
  cleanRecord: [],
}, {
  prefix: "APPLICATION_",
})
