import { createReducer } from "reduxsauce";
import { Types, Creators } from "./actionTypes";
import { fromJS } from "immutable";
import reducers from "./reducers";
import { TypesLib } from "../../lib";

export const ApplicationTypes = Types;
export const ApplicationActions = Creators;

export const INITIAL_STATE: TypesLib.ImmutedApplicationState = fromJS({
  locale: "en",
  button: "",
  record: [],
})

export const ApplicationReducer = createReducer(INITIAL_STATE, {
  [Types.INIT_APPLICATION]: reducers.initApplication,
  [Types.POST_TYPE]: reducers.postType,
  [Types.UPDATE_RECORD]: reducers.updateRecord,
  [Types.CLEAN_RECORD]: reducers.cleanRecord,
});

export default ApplicationReducer;