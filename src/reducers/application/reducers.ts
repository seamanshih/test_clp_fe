import { Reducer } from "redux";
import { TypesLib } from "../../lib";

const initApplication: Reducer = (state: TypesLib.ImmutedApplicationState, action) => {
  const { locale } = action;
  return state.merge({
    locale,
  });
}

const postType: Reducer = (state: TypesLib.ImmutedApplicationState, action) => {
  const { button } = action;
  return state.merge({
    button,
  });
}

const updateRecord: Reducer = (state: TypesLib.ImmutedApplicationState, action) => {
  const { data } = action;
  return state.merge({
    record: data.gameAdded,
  });
}

const cleanRecord: Reducer = (state: TypesLib.ImmutedApplicationState) => {
  return state.merge({
    record: [],
  });
}

export default {
  initApplication,
  postType,
  updateRecord,
  cleanRecord
}