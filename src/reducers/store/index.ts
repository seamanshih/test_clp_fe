import { createStore, applyMiddleware, Store } from 'redux';
import { MakeStore, createWrapper } from 'next-redux-wrapper';
import createSagaMiddleware, { Task } from 'redux-saga';
import rootReducer, { rootState } from "../../reducers";
import rootSaga from "../../sagas";
import { fromJS } from 'immutable';
import { composeWithDevTools } from "redux-devtools-extension";

export interface SagaStore extends Store {
    sagaTask?: Task;
}

// create a makeStore function
const makeStore: MakeStore = () => {
    // 1: Create the middleware
    const sagaMiddleware = createSagaMiddleware();
    // 2: Add an extra parameter for applying middleware:
    const store = createStore(
        fromJS(rootReducer),
        composeWithDevTools(applyMiddleware(sagaMiddleware))
    );
    // 3: Run your sagas on server
    (store as SagaStore).sagaTask = sagaMiddleware.run(rootSaga);
    // 4: now return the store:
    return store;
};

// export an assembled wrapper
export const wrapper = createWrapper<rootState>(makeStore, {
    serializeState: state => state.toJS(),
    deserializeState: state => fromJS(state),
    debug: true
});