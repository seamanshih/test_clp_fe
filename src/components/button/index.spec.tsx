import jest from 'jest';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });
import Button from './index';

jest.mock('react-i18next', () => ({
    useTranslation: () => ({ t: (key: string) => key })
}));

describe('Button', () => {
    it('should render button page', function () {
        const wrap = mount(<Button />);
        expect(wrap.find('div').text()).toBe('Button');
    })
})