import { call, put, } from 'redux-saga/effects';
import { Api } from "../../services/api";

export function* postType(api: Api, action: any) {
   try {
      const button = yield call(api.postType, action.button);
      yield put({ type: "BUTTON_POST_SUCCEEDED", button: button });
   } catch (error) {
      yield put({ type: "BUTTON_POST_FAILED", message: error.message });
   }
}

export function* postClean(api: Api) {
   try {
      yield call(api.postClean);
      yield put({ type: "CLEAN_POST_SUCCEEDED" });
   } catch (error) {
      yield put({ type: "CLEANPOST_FAILED", message: error.message });
   }
}
