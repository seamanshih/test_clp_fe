import { all, takeLatest } from "redux-saga/effects";
import * as applicationReducer from "../reducers/application";
import * as applicationSaga from "../sagas/application";
import { basicApi } from "../services/api";

export function* rootSaga() {
  yield all([
    takeLatest(applicationReducer.ApplicationTypes.POST_TYPE, applicationSaga.postType, basicApi),
    takeLatest(applicationReducer.ApplicationTypes.CLEAN_RECORD, applicationSaga.postClean, basicApi),
  ])
}

export default rootSaga;