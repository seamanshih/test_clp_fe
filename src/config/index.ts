import getConfig from "next/config";

const nextConfig = { ...getConfig().publicRuntimeConfig, ...getConfig().serverRuntimeConfig };

const config = {
  ENV: nextConfig.ENV,
  API: nextConfig.API,
  WS: nextConfig.WS,
};

export default config;
