const nextRuntimeDotenv = require("next-runtime-dotenv");

const withConfig = nextRuntimeDotenv({
  public: [
    "PORT",
    "ENV",
    "API",
    "WS"
  ],
  server: ["API_CLIENT_SECRET", "WS"]
});

module.exports = withConfig({

})