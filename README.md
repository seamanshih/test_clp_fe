## Description

CLP test
build in nextjs and Redux

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn dev

# production mode
$ yarn start
```

## Test

```bash
# unit tests
$ yarn run test
```