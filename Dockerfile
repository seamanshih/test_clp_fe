FROM node:13.14 as builder

WORKDIR /app

COPY . .

RUN yarn install \
  && yarn run build

FROM node:13.14-alpine

WORKDIR /app

COPY package.json yarn.lock . ./

RUN yarn install --production

COPY --from=builder /app/.next ./next

COPY --from=builder /app/node_modules ./node_modules

EXPOSE 3000

CMD [ "yarn", "start" ]

